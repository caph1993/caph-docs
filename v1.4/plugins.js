resources = [
  {
    ref: 'caph-docs/plugins/fabric-editor/fabric-editor.js',
    dependencies: [
      'plugin-codemirror',
      'plugin-fabric',
      'plugin-splitjs',
    ],
  },
  {
    ref: 'plugin-codemirror',
    dependencies: [
      'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js',
      'caph-docs/libraries/codemirror-5.55.0/mode/javascript/javascript.js',
      'caph-docs/libraries/codemirror-5.55.0/addon/search/searchcursor.js',
      'caph-docs/libraries/codemirror-5.55.0/addon/search/search.js',
      'caph-docs/libraries/codemirror-5.55.0/addon/scroll/scrollpastend.js',
      'caph-docs/libraries/codemirror-5.55.0/keymap/sublime.js',
    ]
  },
  {
    ref: 'plugin-splitjs',
    dependencies: [
      'caph-docs/libraries/split/split.js',
    ],
  },
  {
    ref: 'caph-docs/libraries/split/split.js',
  },
  {
    ref: 'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js',
  },
  {
    ref: 'caph-docs/libraries/fabric/fabric.js',
  },
  {
    ref: 'caph-docs/libraries/codemirror-5.55.0/mode/javascript/javascript.js',
    dependencies: [
      'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js'
    ],
  },
  {
    ref: 'caph-docs/libraries/codemirror-5.55.0/addon/search/searchcursor.js',
    dependencies: [
      'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js'
    ],
  },
  {
    ref: 'caph-docs/libraries/codemirror-5.55.0/addon/search/search.js',
    dependencies: [
      'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js'
    ],
  },
  {
    ref: 'caph-docs/libraries/codemirror-5.55.0/addon/scroll/scrollpastend.js',
    dependencies: [
      'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js'
    ],
  },
  {
    ref: 'caph-docs/libraries/codemirror-5.55.0/keymap/sublime.js',
    dependencies: [
      'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js'
    ],
  },
  {
    ref: 'caph-docs/plugins/fabric-editor/fabric-editor.js',
    dependencies: [],
  },
  {
    ref: 'caph-docs/libraries/split/split.css',
    dependencies: [],
  },
  {
    ref: 'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.css',
    dependencies: [
      'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js'
    ],
  },
  {
    ref: 'caph-docs/libraries/codemirror-5.55.0/theme/monokai.css',
    dependencies: [
      'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js'
    ],
  },
]

[
  // Scripts are not loaded if not used :)
  // High priority plugins are loaded first. default: 0
  {
    css_class: "random-bit",
    resources: [],
    parser: async (c)=>(await this.parse_random_bit(c)),
  },

  {
    css_class: "graphviz",
    parser: async (c)=>(await this.parse_graphviz(c)),
    resources: [
      {
        ref: 'caph-docs/libraries/viz/viz.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/libraries/viz/full.render.js',
        dependencies: [
          'caph-docs/libraries/viz/viz.js',
        ],
      },
    ],
  },
  {
    css_class: "fabric",
    parser: async (c)=>(await pluginFabric.parse(c)),
    resources: [
      {
        ref: 'caph-docs/libraries/mathjax2svg/tex-svg.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/libraries/fabric/fabric.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/plugins/fabric/fabric.js',
        dependencies: [
          'caph-docs/libraries/fabric/fabric.js',
        ],
      },
    ],
  },
  {
    css_class: "d3",
    parser: async (c)=>(await this.parse_d3(c)),
    resources: [
      {
        ref: 'caph-docs/libraries/d3/d3.v5.min.js',
        dependencies: [],
      },
    ],
  },
  {
    css_class: "fabric-whiteboard",
    parser: async (c)=>(await pluginWhiteboard.init(this, c)),
    resources: [
      {
        ref: 'caph-docs/libraries/fabric/fabric.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/plugins/fabric-whiteboard/whiteboard.js',
        dependencies: [
          'caph-docs/libraries/fabric/fabric.js',
        ],
      },
      {
        ref: 'caph-docs/plugins/fabric-whiteboard/whiteboard.css',
        dependencies: [],
      },
    ],
    priority: -1,
  },
  {
    css_class: "fabric-editor",
    parser: async (c)=>(await pluginFabricEditor.init(this, c)),
    resources: [
      {
        ref: 'caph-docs/libraries/split/split.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/libraries/fabric/fabric.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/libraries/codemirror-5.55.0/mode/javascript/javascript.js',
        dependencies: [
          'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js'
        ],
      },
      {
        priority: 2,
        ref: 'caph-docs/libraries/codemirror-5.55.0/addon/search/searchcursor.js',
        dependencies: [],
      },
      {
        priority: 2,
        ref: 'caph-docs/libraries/codemirror-5.55.0/addon/search/search.js',
        dependencies: [],
      },
      {
        priority: 2,
        ref: 'caph-docs/libraries/codemirror-5.55.0/addon/scroll/scrollpastend.js',
        dependencies: [],
      },
      {
        priority: 2,
        ref: 'caph-docs/libraries/codemirror-5.55.0/keymap/sublime.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/plugins/fabric-editor/fabric-editor.js',
        dependencies: [],
      },
      {
        ref: 'caph-docs/libraries/split/split.css',
        dependencies: [],
      },
      {
        ref: 'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.css',
        dependencies: [],
      },
      {
        ref: 'caph-docs/libraries/codemirror-5.55.0/theme/monokai.css',
        dependencies: [],
      },
      {
        ref: 'caph-docs/plugins/fabric-editor/fabric-editor.css',
        dependencies: [],
      },
    ],
    priority: -1,
  },
  {
    css_class: "blockquote",
    parser: (c)=>null,
    resources: [
      {
        ref: 'caph-docs/plugins/blockquote/blockquote.css',
        dependencies: [],
      },
    ],
    priority: 9,
  },
  {
    css_class: "parent-center",
    parser: async (c)=>(await pluginParentCenter.parse(c)),
    resources: [
      {
        ref: 'caph-docs/plugins/parent-center/parent-center.js',
        dependencies: [],
      },
    ],
    priority: 10,
  },
  {
    css_class: "collapsible",
    parser: async (c)=>(await this.parse_collapsible(c)),
    priority: 9,
  }
]