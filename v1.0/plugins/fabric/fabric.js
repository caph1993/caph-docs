class PluginFabric{
  
  constructor(hidden=true){
    this.loaded = {};
    this.already = false;
    this.hidden = hidden;
  }
  async init(container){
    if(this.already) return;
    this.already=true;
    let main = document.createElement('div', {id:'#whiteboard-main'});
    main.hidden = this.hidden;
    main.classList.add('whiteboard-main');
    main.innerHTML = this.controlsHTML;
    Slides.add_menu_option('Whiteboard', {
      show: ()=>(main.hidden=this.hidden=false),
      hide: ()=>(main.hidden=this.hidden=true),
    });
    container.appendChild(main);
    try{
      await this.whiteboard_init();
    } finally{
      let canvas = this.__canvas;
      canvas.setHeight(3*window.innerHeight);
      canvas.setWidth(3*window.innerWidth);
    }
  }

  async parse(container){
    if(container.tagName!='SCRIPT') return;
    let canvas = MyDocument.createElement('canvas', {
      parent: container, where: 'afterend',
      classList: container.classList,
    });
    let fabric_canvas = new fabric.Canvas(canvas);
    let script = container.innerText;
    var f = eval(script);
    return await f(fabric_canvas);
  }


}

var pluginFabric = new PluginFabric();






class CaphFabric{}
CaphFabric._Base = class{
  container = null;
  async draw_on(canvas, {width, height, top=0, left=0, border=false}={}){
    assert(width&&height);
    let reg = {}
    await this.load(reg);
    canvas.setHeight(width+2);
    canvas.setWidth(height+2);
    this._draw_on(canvas, reg, {width, height, top, left, border});
  }
  make_border(){
    return new fabric.Rect({
      fill: '#eeffdd11', stroke:'black',
      width: this.container.width, height: this.container.height,
      top: this.container.top, left: this.container.left,
    });
  }
  get center(){
    return {
      x: this.container.left+this.container.width/2,
      y: this.container.top+this.container.height/2,
    }
  }
}

CaphFabric.Atom = class extends CaphFabric._Base{
  element=null;
  async load(reg){
    await this.load_box();
    if(this.id) reg[this.id]=this;
  }
  _draw_on(canvas, reg, {width, height, top, left, border}){
    this.container = {width, height, top, left};
    if(border) canvas.add(this.make_border());
    this.element.set({ // center
      top: top+(height-this.height)/2,
      left: left+(width-this.width)/2,
    });
    canvas.add(this.element);
  }

  async load_svg(svg_string){
    return new Promise((ok, err)=>{
      fabric.loadSVGFromString(svg_string, (objs, opts)=>{
        var obj;
        try{ obj = fabric.util.groupSVGElements(objs, opts);}
        catch(e){ err(e); }
        if(obj){
          ok(obj);
        }
      });
    });
  }

  async load_tex(formula){
    formula = formula||'';
    //console.log(MathJax.svgStylesheet());
    const e = MathJax.tex2svg(formula).firstElementChild;
    let k = document.fonts.size/2;
    let width = k*e.width.baseVal.valueInSpecifiedUnits;
    let height = k*e.height.baseVal.valueInSpecifiedUnits;
    let text = await this.load_svg(e.outerHTML);
    text.set({
      scaleX: width/text.width,
      scaleY: height/text.height,
    });
    return text;
  }

  async load_box(){
    let text = await this.load_tex(this.text);
    
    let padding=null, margin=null, fill='#ffeeaa', stroke='black', box_props={};

    let width = text.width*text.scaleX;
    let height = text.height*text.scaleY;
    if(padding==null) padding = 5+Math.log1p(height*width);
    if(margin==null) margin = 0;
    text.set({top:margin+padding, left:margin+padding});

    let rect = new fabric.Rect({
      left:margin, top:margin,
      width: width+2*padding,
      height: height+2*padding,
      fill:fill, stroke:stroke, ...box_props
    });
    let box = new fabric.Group([rect, text]);
    box.set({
      width: 2*margin+rect.width,
      height: 2*margin+rect.height,
    });
    this.element = box;
  }
  get width(){ return this.element.width*this.element.scaleX; }
  get height(){ return this.element.height*this.element.scaleY; }
  get left(){ return this.element.left; }
  get top(){ return this.element.top; }
  side_midpoint(side){
    switch (side) {
      case 'top':
        return [this.left+this.width/2, this.top];
      case 'bottom':
        return [this.left+this.width/2, this.top+this.height];
      case 'left':
        return [this.left, this.top+this.height/2];
      case 'right':
        return [this.left+this.width, this.top+this.height/2];
      default:
        throw new Error();
    }
  }
  get center(){
    return [this.left+this.width/2, this.top+this.height/2];
  }

}


CaphFabric.Leaf = class extends CaphFabric.Atom{
  arrows = [];
  constructor({text='', id='', width=null, height=null,
      padding=null, fill='#ffeeaa88', stroke='black',
      top=null, left=null, ...box_props}={}){
    super();
    this.text=text;
    this.id=id;
  }
}

CaphFabric.Arrow = class extends CaphFabric.Atom{
  constructor({from, to, text, stroke, ...props}={}){
    assert(from && from.id && from.side);
    assert(to && to.id && to.side);
    super();
    this.from=from;
    this.to=to;
    this.text=text;
    this.stroke = stroke||'black';
    this.props=props;
  }
  _draw_on(canvas, reg){
    this.from.leaf = reg[this.from.id];
    this.to.leaf = reg[this.to.id];
    this.from.point = ()=>this.from.leaf.side_midpoint(this.from.side);
    this.to.point = ()=>this.to.leaf.side_midpoint(this.to.side);

    let [x1,y1] = this.from.point();
    let [x2,y2] = this.to.point();
    let line = new fabric.Line([x1,y1,x2,y2],{
      stroke: this.stroke, selectable:false,
    });

    this.from.leaf.element.on('moving', (e)=>{
      let [x1,y1] = this.from.point();
      line.set({x1, y1});
    });
    this.to.leaf.element.on('moving', (e)=>{
      let [x2,y2] = this.to.point();
      line.set({x2, y2});
    });
    canvas.add(line);

    let r=5;
    let originX, originY;
    let angle = Math.atan2((y1-y2),(x1-x2))*180/Math.PI-90;
    let deltaX=-r*Math.sin(angle*Math.PI/180);
    let deltaY=r*Math.cos(angle*Math.PI/180);

    let triangle = new fabric.Triangle({
        left: x2 + deltaX,
        top: y2 + deltaY,
        originX: 'center',
        originY: 'center',
        selectable: false,
        pointType: 'arrow_start',
        angle: angle,
        width: 2*r, height: 2*r,
        fill: this.stroke,
    });
    canvas.add(triangle);
  }
}

CaphFabric.Tree = class extends CaphFabric._Base{
  direction = 'row'; //only row or column

  constructor(...children){
    super();
    this.children = this.validate_trees(children);
    this.arrows = [];
  }
  get elements(){
    return this.children.map(x=>x.element);
  }
  async load(reg){
    for(let e of this.children) await e.load(reg);
    for(let a of this.arrows) await a.load(reg);
    if(this.id) reg[this.id] = this;
  }
  validate_trees(trees){
    for(let e of trees){
      if(e instanceof CaphFabric.Tree) ;
      else if(e instanceof CaphFabric.Leaf);
      else{
        console.error(e);
        throw 'Object must be Tree or Leaf';
      }
    }  
    return trees;
  }
  set({direction}={}){
    this.direction = direction||this.direction;
    return this;
  }
  add_edges(...arrows){
    arrows.forEach(e=>this.arrows.push(e));
    return this;
  }

  _draw_on(canvas, reg, {width, height, top, left, border}){
    this.container = {width, height, top, left};
    if(border) canvas.add(this.make_border());
    let w, h, dw, dh, aux;
    if(this.direction=='row'){
      let W = this.width;
      w = dw = (e)=>e.width/W*width;
      h = (e)=>height;
      dh = (e)=>0;
    } else if (this.direction=='column'){
      let H = this.height;
      h = dh = (e)=>e.height/H*height;
      w = (e)=>width;
      dw = (e)=>0;
    } else throw new Error();

    for(let e of this.children){
      e._draw_on(canvas, reg, {top: top, left: left, width:w(e), height:h(e), border });
      left+=dw(e); top+=dh(e);
    }
    this.arrows.forEach((a)=> a._draw_on(canvas, reg));
  }

  get width(){
    let v;
    if(this.direction=='row')
      v = this.children.reduce((p,c)=>p+c.width, 0);
    else if(this.direction=='column')
      v = this.children.reduce((p,c)=>Math.max(p,c.width), 0);
    else throw new Error();
    return v;
  }
  get height(){
    let v;
    if(this.direction=='column')
      v = this.children.reduce((p,c)=>p+c.height, 0);
    else if(this.direction=='row')
      v = this.children.reduce((p,c)=>Math.max(p,c.height), 0);
    else throw new Error();
    return v;
  }
}
CaphFabric.HTree = CaphFabric.Tree;
CaphFabric.VTree = class extends CaphFabric.Tree{
  direction='column';
}
