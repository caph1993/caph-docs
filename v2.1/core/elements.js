// htm syntax

caph.makePlugin = ({component, loader=null, post_loader=null})=>{
  return function(){
    const [ready, setReady] = preact.useState(false);
    const [error, setError] = preact.useState(null);
    const _load = async ()=>{
      try{
        if(loader) await loader(...arguments);
        setReady(true);
        if(post_loader) await post_loader(...arguments);
      } catch(err){ setError(err); console.error(err); }
    };
    preact.useEffect(()=>{_load();}, []);
    return html`${
      error? html`<div>Error</div>`
      : (
        ready? component.apply(this, arguments)
        : html`
          <div class="hbox align-center space-around flex"
              style="width:100%;height:100%">
            <div class="plugin-loading"/>
          </div>`
      )
    }`;
  }
}


const CaphSlides = caph.makePlugin({
  component: ({children, theme})=>{
    return html`
    <div class="reveal">
      <div class="slides">
        ${children}
      </div>
    </div>
  `},
  loader: async({theme})=>{
    await caph.load('caph-docs/reveal.js/dist/reveal.js');
    await caph.load('caph-docs/reveal.js/dist/reveal.css');
    if(theme=='marp') caph.load('caph-docs/core/marp.css');
    else if(theme) caph.load(`caph-docs/reveal.js/dist/theme/${theme}.css`);
    await caph.load('caph-docs/reveal.js/plugin/notes/notes.js');
    await caph.load('caph-docs/reveal.js/plugin/markdown/markdown.js');
    await caph.load('caph-docs/reveal.js/plugin/highlight/highlight.js');
    if(!document.getElementById('theme')){
      await caph.load('caph-docs/reveal.js/dist/theme/simple.css', {
        id: 'theme',
        parent: caph.head_sources,
      });
    }
    if(!document.getElementById('highlight-theme')){
      await caph.load('caph-docs/reveal.js/plugin/highlight/monokai.css',{
        id: 'highlight-theme',
        parent: caph.head_sources,
      });
    }
    await caph.load('caph-docs/slides-core.css',{
      parent: caph.head_sources,
    });
    return;
  },
  post_loader: async({reveal_options})=>{
    let print = (window.location.href.indexOf('?print-pdf')!=-1);
    let options = MyObject.deep_assign({
      progress: true,
      slideNumber: 'c/t',
      history: true,
      //center: true,
      //width: '100%', height: '100%',
      hash: true,
      transition: 'none', // none/fade/slide/convex/concave/zoom
      //plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ],
      keyboard: {
        // Check codes here https://keycode.info/
        //87: ()=> this.set_menu_option('Whiteboard'),// W key
        //27: ()=> this.set_menu_option('Default'),// esc key
      }
    }, reveal_options);
    if(print){
      options.width=options.height='100%';
      center=true;
    }

    await MyPromise.until(()=>
      document.querySelector('.reveal')
      && document.querySelector('.slides')
    );
    Reveal.initialize(options);
    await MyPromise.until(()=>Reveal.isReady());
    if(print) window.print();
    caph.menu.pushOption('PDF print', {
      show:()=>{
        let loc = window.location;
        open(loc.href.slice(0,-loc.hash.length)+'?print-pdf');
        caph.menu.onSelect();
      },
    });
    return;
  }
});


caph.scroll = new class {
  constructor(){
    // restore previous scroll position if available
    window.addEventListener('keydown', (e)=>{
      if (e.keyCode == 116) this.save_scroll_position(); // F5 key
    });
  }
  save_scroll_position(){
    window.localStorage.setItem('scrollX', ''+window.scrollX);
    window.localStorage.setItem('scrollY', ''+window.scrollY);
  }
  load_scroll_position(){
    let scrollX = parseInt(window.localStorage.getItem('scrollX'));
    let scrollY = parseInt(window.localStorage.getItem('scrollY'));
    window.scrollBy(scrollX-window.scrollX, scrollY-window.scrollY);
  }
  load_href_scroll_position(){
    let href =  window.location.href;
    if (href.indexOf('startAt') != -1 ) {
      let match = href.split('?')[1].split("&")[0].split("=");
      document.getElementsByTagName("body")[0].scrollTop = match[1];
    }
  }
  async initial_scroll(ms=200, ms_stable=3000){
    // scrolls to last saved scroll position every ms until the
    // document height is stable for at least ms_stable
    // fights scroll unstability after each visible plugin loads 
    let t=0, h, prevh, initialh;
    do{
      prevh=initialh=document.body.scrollHeight;
      for(let t=0; t<ms_stable; t+=ms){
        await sleep(ms);
        h = document.body.scrollHeight;
        if(h!=prevh){ prevh=h; this.load_scroll_position();}
      }
    } while(prevh!=initialh);
  }
}

const CaphDocument = caph.makePlugin({
  component: ({children})=>{
    return html`<div id="caph-body" class="body hyphenate">${children}</div>`
  },
  loader: async({theme})=>{
    await caph.load('caph-docs/core/document.css');
    caph.menu.pushOption('PDF print', {
      show:()=>{window.print(); caph.menu.onSelect();},
    });
    return;
  },
  post_loader: async({theme})=>{
    let main = await MyPromise.until(()=>
      document.querySelector('#caph-body')
    );
    await caph.load('caph-docs/libraries/Hyphenator-5.3.0/Hyphenator.min.js');
    caph.scroll.initial_scroll();
    return;
  },
});


const MathJaxSvg = caph.makePlugin({
  component: ({children})=>{
    return html`${children}`;
  },
  loader: async()=>{
    window.MathJax = MyObject.deep_assign({
      tex: {
        inlineMath: [['$', '$'], ['\\(', '\\)']],
        macros:{ RR: "{\\mathbb R}", }
      },
      svg: {fontCache: 'local', exFactor: 1.0, scale: 0.9,},
    }, window.MathJax||{});
  },
  post_loader: async()=>{
    if(window.Reveal) await MyPromise.until(()=>Reveal.isReady());
    await caph.load('caph-docs/libraries/mathjax2svg/tex-svg.js', {
      id: 'MathJax-script',
    });
  },
});


const PluginWhiteboard = caph.makePlugin({
  component: ({children})=>{
    if(children&&children.length) throw 'Unexpected children';
    return html`
<div id="whiteboard-main" class="fullscreen-layer hidden">
  <canvas id="whiteboard-canvas" class="whiteboard-canvas" />
  <div id="whiteboard-background" class="whiteboard-background" />
  <div id="whiteboard-controls" class="whiteboard-controls hbox">
    <button id="clear-canvas" class="btn btn-info">Clear</button>
    <button id="drawing-mode" class="btn btn-info">🖱</button>
    <div id="drawing-mode-options" class="hbox">
      <select id="drawing-mode-selector">
        <option>Pencil</option>
        <option>Spray</option>
        <option>Circle</option>
        <option>Pattern</option>
        <option>hline</option>
        <option>vline</option>
        <option>square</option>
        <option>diamond</option>
      </select>
      <input type="color" value="#005E7A" id="drawing-color" />
      <input type="range" value="50" min="0" max="150" id="drawing-line-width" />
      ❏
      <input type="range" value="10" min="2" max="50" id="drawing-shadow-width" />
    </div>
    <div id="non-drawing-mode-options" class="hbox">
      <select id="non-drawing-mode-selector" value="Select">
        <option>Select</option>
        <option>Text</option>
        <option>Pointer</option>
        <option>Math?</option>
      </select>
    </div>
  </div>
</div>`;
  },
  loader: async({theme})=>{
    await caph.load('caph-docs/libraries/fabric/fabric.js');
    await caph.load('caph-docs/plugins/whiteboard.css');
    return;
  },
  post_loader: async({theme})=>{
    await caph.load('caph-docs/plugins/whiteboard.js');
    let main = await MyPromise.until(()=>
      document.querySelector('#whiteboard-main')
    );
    caph.menu.pushOption('Whiteboard', {
      show:()=>{main.classList.remove('hidden');main.hidden=false; },
      hide:()=>{main.classList.add('hidden');main.hidden=true; },
    });
    return;
  },
});

const FabricDiagram = caph.makePlugin({
  component: ({children, id=null})=>{
    assert(children&&children.props&&children.props.children);
    let script = (x=>(Array.isArray(x)?x.join(''):x))(
      children.props.children
    );
    let classString = children.props.class||"";
    script = script.replace(/\\/g, '\\\\'); // Escape backslash again
    if(!id) id='diagram-'+Math.floor(1e12*Math.random());
    preact.useEffect(()=>{ plugin(script); }, []);

    const plugin = async(script)=>{
      await MyPromise.until(()=>
        !!document.querySelector(`#${id}`)
      );
      let canvas = document.querySelector(`#${id}`);
      caph.plugins.fabric.render(canvas, script);
    }
    return html`
      <canvas id=${id} tabIndex="1" fireRightClick
          class=${classString}>
      </canvas>
    `;
  },
  loader: async()=>{
    await caph.load('caph-docs/libraries/fabric/fabric.js');
    await caph.load('caph-docs/libraries/mathjax2svg/tex-svg.js');
    await caph.load('caph-docs/plugins/fabric-diagram.js');
    await caph.load('caph-docs/plugins/fabric.css');
    await caph.load('caph-docs/plugins/fabric.js');
    return;
  },
});


const Codemirror = caph.makePlugin({
  component: ({children, id=null, cm_options={}})=>{
    assert(children&&children.props&&children.props.children);
    let script = (x=>(Array.isArray(x)?x.join(''):x))(
      children.props.children
    );
    let classString = children.props.class||"";
    if(!id) id='codemirror-'+Math.floor(1e12*Math.random());
    preact.useEffect(()=>{ plugin(script, cm_options); }, []);

    const plugin = async(script, cm_options)=>{
      let div = await MyPromise.until(()=>
        document.querySelector(`#${id}`));
      caph.plugins.codemirror.render(div, script, cm_options);
    }
    return html`<div id=${id} class=${classString}/>`;
  },
  loader: async()=>{
    await caph.load('caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/mode/javascript/javascript.js');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/mode/python/python.js');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/addon/search/searchcursor.js');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/addon/search/search.js');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/addon/scroll/scrollpastend.js');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/keymap/sublime.js');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/addon/display/autorefresh.js');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/lib/codemirror.css');
    await caph.load('caph-docs/libraries/codemirror-5.55.0/theme/monokai.css');
    await caph.load('caph-docs/plugins/codemirror.css');
    await caph.load('caph-docs/plugins/codemirror.js');
    return;
  },
});
