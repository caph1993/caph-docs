
function assert(condition, ...messages){
  if(condition) return;
  throw new Error(messages);
}

function sleep(ms){
  return new Promise((ok,err)=>setTimeout(ok, ms));
}

class MyObject{

  static indexBy(arr, key){
    let M = {};
    arr.forEach((obj)=>{
      let value = obj[key];
      M[value] = obj;
    });
    return M;
  }

  /**
  @param {((value:any, key?:any, obj?:any)=>(any))} func
  */
  static map(obj, func){
    return MyObject._object_op('map', obj, func);
  }

  /**
  @param {((value:any, key?:any, obj?:any)=>(any))} func
  */
  static filter(obj, func){
    return MyObject._object_op('filter', obj, func);
  }

  /**
  @param {((value:any, key?:any, obj?:any)=>(any))} func
  */
  static apply(obj, func){
    return MyObject._object_op('apply', obj, func);
  }

  /**
  @param {((value:any, key?:any, obj?:any)=>(any))} func
  */
  static forEach(obj, func){
    return MyObject._object_op('forEach', obj, func);
  }

  static _object_op(op, src, func){
    assert(1<=func.length && func.length<=3);
    let dest = {};
    for(let key of Object.keys(src)){
      let value = src[key]
      let result = func.length==1? func(value) : func.length==2?
        func(value,key) : func(value, key, src);
      if(op=='map') dest[key] = result;
      else if(op=='filter'){ if(result) dest[key] = value; }
      else if(op=='apply') src[key] = result;
      else if(op=='forEach') 1==1;
    }
    return (op=='map'||op=='filter')? dest : null;
  }

  static reduce_dots(obj){
    // converts {'a.b':1, 'a.c':2, b:3} into {a:{b:1,c:2}, 'b':3}. Returns copy (shallow or deep)
    let dotted = MyObject.object_filter(obj, (v,k)=>k.indexOf('.')>=0);
    let no_dots = MyObject.object_filter(obj, (v,k)=>k.indexOf('.')==-1);
    if(Object.keys(dotted).length==0) return no_dots;
    MyObject.object_forEach(dotted, (v,k)=>{
      let start = k.slice(0, k.indexOf('.'));
      let end = k.slice(k.indexOf('.')+1);
      no_dots[start] = no_dots[start]||{};
      no_dots[start][end] = v;
    });
    return MyObject.reduce_dots(no_dots);
  }

  static deep_copy(obj){
    if(obj===undefined) return undefined;
    if(obj===null) return null;
    if(Array.isArray(obj)) return obj.map(x=>deep_copy(x));
    if(typeof(obj)=="object") return MyObject.object_map(obj, value=>MyObject.deep_copy(value));
    if(typeof(obj)=="function") return obj; // DOES NOT CREATE COPY FOR FUNCTIONS
    return obj;
  }

  static deep_assign(obj, ...objs){
    for(let o of objs){
      for(let key of Object.keys(o)){
        if(obj[key]===undefined || typeof(o[key])!="object") obj[key] = o[key];
        else MyObject.deep_assign(obj[key], o[key]);
      }
    }
    return obj;
  }
}

function all_true(arr){
  for(let x of arr) if(!x) return false;
  return true;
}
function any_true(arr){
  for(let x of arr) if(x) return true;
  return false;
}
function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;
  for(let i=0; i<a.length; ++i){
    if (a[i] !== b[i]) return false;
  }
  return true;
}


function Promise_finish_all(promises){
  // Ensures completion of all promises even if some throw exceptions
  return new Promise((ok, err)=>{
    if(promises.length==0) return ok([]);
    let any_error=false;
    let cnt = 0;
    let outs = promises.map((p)=>null);
    Promise.all(promises.map(async (p, i)=>{
      try{ outs[i] = await p; }
      catch(e){ any_error=true; outs[i] = e; }
      cnt += 1
      if(cnt==promises.length){
        if(any_error) err(outs);
        else ok(outs);
      }
    }));
  });
}

function get_property_handler(object, property){
  let access, proto = object;
  object._hidden_modified = false;
  while(!access){
    proto = Object.getPrototypeOf(proto);
    access = Object.getOwnPropertyDescriptor(proto, property);
  }
  return access;
}

function update_property_handler(object, property, create_handler){
  let prev = get_property_handler(object, property);
  Object.defineProperty(object, property, create_handler(prev));
}

let KEY_CODES = {
  "0": 48, "1": 49, "2": 50, "3": 51, "4": 52,
  "5": 53, "6": 54, "7": 55, "8": 56, "9": 57, "backspace": 8,
  "tab": 9, "enter": 13, "shift": 16, "ctrl": 17, "alt": 18,
  "pause_break": 19, "caps_lock": 20, "escape": 27, "page_up": 33,
  "page_down": 34, "end": 35, "home": 36, "left_arrow": 37,
  "up_arrow": 38, "right_arrow": 39, "down_arrow": 40, "insert": 45,
  "delete": 46, "a": 65, "b": 66, "c": 67, "d": 68, "e": 69, "f": 70,
  "g": 71, "h": 72, "i": 73, "j": 74, "k": 75, "l": 76, "m": 77,
  "n": 78, "o": 79, "p": 80, "q": 81, "r": 82, "s": 83, "t": 84,
  "u": 85, "v": 86, "w": 87, "x": 88, "y": 89, "z": 90,
  "left_window_key": 91, "right_window_key": 92, "select_key": 93,
  "numpad_0": 96, "numpad_1": 97, "numpad_2": 98, "numpad_3": 99,
  "numpad_4": 100, "numpad_5": 101, "numpad_6": 102, "numpad_7": 103,
  "numpad_8": 104, "numpad_9": 105, "multiply": 106, "add": 107,
  "subtract": 109, "decimal_point": 110, "divide": 111,
  "f1": 112, "f2": 113, "f3": 114, "f4": 115, "f5": 116, "f6": 117,
  "f7": 118, "f8": 119, "f9": 120, "f10": 121, "f11": 122, "f12": 123,
  "num_lock": 144, "scroll_lock": 145, "semi_colon": 186,
  "equal_sign": 187, "comma": 188, "dash": 189, "period": 190,
  "forward_slash": 191, "grave_accent": 192, "open_bracket": 219,
  "back_slash": 220, "close_braket": 221, "single_quote": 222
}

class MyDocument{
  static createElement(tag, {
      style={}, id={}, classList=[], text=null, html=null,
      eventListeners={}, parent=null, where=null, ...attrs}={}){
    let e = document.createElement(tag, id?{id:id}:null);
    classList.forEach(s=>e.classList.add(s));
    if(text!=null) e.innerText = text;
    if(html!=null) e.innerHTML = html;
    MyObject.forEach(attrs, (value, key)=>e.setAttribute(key, value));
    MyObject.forEach(style, (value, key)=>e.style[key] = value);
    MyObject.forEach(eventListeners, (value, key)=>
      e.addEventListener(key, value));
    if(parent || where){
      parent = parent||document.body;
      where = where||'beforeend';
      parent.insertAdjacentElement(where, e);
    }
    return e;
  }
}


class SlidesCore{
  constructor(){
    this.loaded = {};
    let element = document.getElementById('caph-slides');
    assert(element, 'No id found. Load this using <script id="caph-slides" src="...">')
    this.head_sources = document.createElement('div');
    element.insertAdjacentElement('afterend', this.head_sources);
    this.load('caph-slides/reveal.js/dist/reset.css', {parent:this.head_sources});
    this.load('caph-slides/reveal.js/dist/reveal.css', {parent:this.head_sources});
    this.already = false;
    window.addEventListener('load', ()=>this._init())
  }

  async _init(){
    if(this.already) return;
    this.already = true;
    await this.load_script('caph-slides/reveal.js/dist/reveal.js');
    if(Reveal.isReady()) this._on_reveal_ready();
    else Reveal.on('ready', (e)=> this._on_reveal_ready());
    this.init_menu();
    this.init();
  }

  init_menu(){
    this.menu = document.createElement('select', {id:'slides-menu'});
    this.menu.classList.add('slides-menu');
    document.body.appendChild(this.menu);
    this.menu_options = [];
    this.menu.addEventListener('change', (e)=>{
      let selected = this.menu.value;
      this.menu_options.forEach(({text, show, hide})=>{
        if(text==selected) show();
        else hide();
      });
      if(selected=='Slides') this.menu.classList.remove('slides-menu-shown');
      else this.menu.classList.add('slides-menu-shown');
    });
    this.add_menu_option('Slides');
  }

  add_menu_option(text, {show=()=>null, hide=()=>null}={}){
    this.menu_options.push({text, show, hide});
    let opt = document.createElement('option');
    opt.innerText = text;
    this.menu.insertAdjacentElement('beforeend', opt);
    if(this.menu_options.length>1){
      this.menu.classList.add('slides-menu-multiple');
    }
  }

  __load_elem(tag, attrs, parent, where){
    return new Promise((ok, err)=>{
      let e = document.createElement(tag);
      let done = false;
      e.onload = ()=>(done=true) && ok();
      e.onerror = (x)=>(done=true) && err(x); // HTTP errors only
      Object.keys(attrs).map(key =>
        e.setAttribute(key, attrs[key])
      );
      parent.insertAdjacentElement(where, e);
      setTimeout(()=>done||err(['Timeout (3s) loading source:', e]), 3000);
    });
  };

  async _load_elem(ref, tag, attrs, parent, where){
    // Handle concurrent calls to load_elem(...) about the same ref
    if(!this.loaded[ref]){
      this.loaded[ref]=1;
      try{
        await this.__load_elem(tag, attrs, parent, where);
        this.loaded[ref]=2;
      } catch(err){
        this.loaded[ref]=0;
        throw err;
      }
    }
    while(this.loaded[ref]==1){ // If being loaded in other thread...
      await sleep(1500);
    }
  }

  async load_script(src){
    await this.load(src, {
      parent: document.body,
      where: 'beforeend',
    });
  };

  async load_style(href, id=null){
    await this.load(href, {
      parent: document.head,
      where: 'beforeend',
      attrs: id?{id:id}:{},
    });
  };

  async load(ref, {attrs={}, parent=null, where='beforeend', auto_attrs=true}={}){
    if(parent==null) parent=document.body;
    const ext = ref.split('.').pop();
    const tag = ext=='js'? 'script': ext=='css'? 'link' : null;
    assert(tag!=null, 'Only .js and .css files can be loaded. Got: '+ext+' '+ref);
    let defaults = {};
    if(auto_attrs && tag=='link') defaults={rel:'stylesheet', type:'text/css'};
    MyObject.deep_assign(defaults, attrs);
    attrs = defaults;
    if(tag=='script') attrs.src = ref;
    if(tag=='link') attrs.href = ref;
    await this._load_elem(ref, tag, attrs, parent, where);
  }

  async _on_reveal_ready(){
    try{ await this.__on_reveal_ready(); }
    finally{ Reveal.sync(); }
  }

  async __on_reveal_ready(){
    // Parse custom css_classes
    const get_elems = (s)=>[...document.getElementsByClassName(s)];
    const custom_parsers = Reveal.getConfig().custom_parsers;
    await Promise_finish_all(custom_parsers.map(async (p)=>{
      // Get all elements of the class
      const css_class = p.css_class;
      const scripts = p.scripts || [];
      const styles = p.styles || [];
      const parser = p.parser || ((_)=>console.log('No parser for', css_class));
      
      // Create loaders in DOM
      const css_loader = 'parser-loading tmp-'+css_class;
      const src_elements = get_elems(css_class);
      if(!src_elements.length) return;

      const was_hidden = src_elements.map(e=>e.hidden);
      let all_diff = (a,b) => all_true(a.map((_,i)=>a[i]!=b[i]));
      await Promise.all(src_elements.map(async e =>{
        let loader = document.createElement('div');
        css_loader.split(' ').map(s=>loader.classList.add(s));
        e.insertAdjacentElement('afterend', loader);
        e.hidden = true;
        return;
      }));
      
      // Wait for elements query to update (this is weird but necessary)
      const ready = () => all_diff(src_elements, get_elems(css_class));
      try{
        await Promise.all([
          (async ()=>{ await sleep(1500); throw 'Timeout'; })(),
          (async ()=>{ while(!ready()) await sleep(5); })(),
        ]);
      } catch(e){ if(e!='Timeout') throw e; }

      const elements = get_elems(css_class);

      // Define restore_hidden_state() for each element
      elements.forEach((e,i)=>{
        e._hidden_modified = false;
        e._was_hidden = was_hidden[i];
        update_property_handler(e, 'hidden', (prev)=>({
          get: function () {
            return prev.get.call(this);
          },
          set: function (val) {
            this._hidden_modified = true;
            return prev.set.call(this, val);
          }
        }));
        e.restore_hidden_state = function(){
          if(!this._hidden_modified) this.hidden=this._was_hidden;
        }
      });

      try{
        // Import scripts and styles in series (dependency order)
        if(elements.length){
          for(let src of styles){await this.load_style(src)};
          for(let src of scripts){await this.load_script(src)};
        }
        // Run parser functions and clear loaders
        await Promise_finish_all(elements.map(async (e) =>{
          try{
            await parser(e);
          } finally {
            e.restore_hidden_state();
            get_elems(css_loader).forEach(x=>x==e.nextSibling?x.remove():null);
          }
        }));
      } finally{
        // Remove possible remaining loaders and unhide possible hidden 
        // This may happen because of the 'liveness' of get_elems
        get_elems(css_loader).forEach(e=>e.remove());
      }
    }));
  }
}

class SlidesClass extends SlidesCore{

  async init(){
    await this.load_script('caph-slides/reveal.js/plugin/notes/notes.js');
    await this.load_script('caph-slides/reveal.js/plugin/markdown/markdown.js');
    await this.load_script('caph-slides/reveal.js/plugin/highlight/highlight.js');

    if(!document.getElementById('theme')){
      await this.load('caph-slides/reveal.js/dist/theme/simple.css', {
        id: 'theme',
        parent: this.head_sources,
      });
    }
    if(!document.getElementById('highlight-theme')){
      await this.load_style('caph-slides/reveal.js/plugin/highlight/monokai.css',{
        id: 'highlight-theme',
        parent: this.head_sources,
      });
    }
    await this.load_style('caph-slides/core.css',{
      parent: this.head_sources,
    });

    Reveal.initialize({
      progress: true,
      slideNumber: 'c/t',
      history: true,
      center: true,
      //width: '100%', height: '100%', hash: true,
      transition: 'none', // none/fade/slide/convex/concave/zoom
      plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ],
      dependencies: [
          {
            src: 'caph-slides/libraries/reveal.js-math-katex-plugin/math-katex.js',
            async: true,
          },
          //{ src: 'caph-slides/libraries/reveal.js-rajgoel-plugins/chalkboard/chalkboard.js' },
      ],
      math:{
        katexScript:     'caph-slides/libraries/katex/katex.min.js',
        katexStylesheet: 'caph-slides/libraries/katex/katex.min.css'
      },
      keyboard: {
        // Check codes here https://keycode.info/
         8: ()=> console.log('key pressed'),
      },
      custom_parsers: [ // Scripts are not loaded if not used :)
        {
          css_class: "random-bit",
          scripts: [],
          styles: [],
          parser: async (c)=>(await this.parse_random_bit(c)),
        },
        {
          css_class: "graphviz",
          parser: async (c)=>(await this.parse_graphviz(c)),
          scripts: [
            'caph-slides/libraries/viz/viz.js',
            'caph-slides/libraries/viz/full.render.js',
          ],
        },
        {
          css_class: "fabric",
          parser: async (c)=>(await pluginFabric.parse(c)),
          scripts: [
            'caph-slides/libraries/fabric/fabric.js',
            'caph-slides/plugins/fabric/fabric.js',
          ],
        },
        {
          css_class: "d3",
          parser: async (c)=>(await this.parse_d3(c)),
          scripts: [
            'caph-slides/libraries/d3/d3.v5.min.js',
          ],
        },
        {
          css_class: "fabric-whiteboard",
          parser: async (c)=>(await pluginWhiteboard.init(c)),
          scripts: [
            'caph-slides/libraries/fabric/fabric.js',
            'caph-slides/plugins/fabric-whiteboard/whiteboard.js',
          ],
          styles: [
            'caph-slides/plugins/fabric-whiteboard/whiteboard.css',
          ]
        },
        {
          css_class: "fabric-editor",
          parser: async (c)=>(await pluginFabricEditor.init(c)),
          scripts: [
            'caph-slides/libraries/split/split.js',
            'caph-slides/libraries/codemirror-5.55.0/lib/codemirror.js',
            'caph-slides/libraries/codemirror-5.55.0/mode/javascript/javascript.js',
            'caph-slides/libraries/codemirror-5.55.0/addon/search/searchcursor.js',
            'caph-slides/libraries/codemirror-5.55.0/addon/search/search.js',
            'caph-slides/libraries/codemirror-5.55.0/addon/scroll/scrollpastend.js',
            'caph-slides/libraries/codemirror-5.55.0/keymap/sublime.js',
            'caph-slides/libraries/fabric/fabric.js',
            'caph-slides/plugins/fabric-editor/fabric-editor.js',
          ],
          styles: [
            'caph-slides/libraries/split/split.css',
            'caph-slides/libraries/codemirror-5.55.0/lib/codemirror.css',
            'caph-slides/libraries/codemirror-5.55.0/theme/monokai.css',
            'caph-slides/plugins/fabric-editor/fabric-editor.css',
          ]
        },
        {
          css_class: "blockquote",
          styles: [
            'caph-slides/plugins/blockquote/blockquote.css',
          ],
        },
        {
          css_class: "slides-center",
          parser: async (c)=>(await pluginSlidesCenter.parse(c)),
          scripts: [
            'caph-slides/plugins/slides-center/slides-center.js',
          ],
        },
        {
          css_class: "collapsible",
          parser: async (c)=>(await this.parse_collapsible(c)),
        }
      ]
    });
  }
  async parse_random_bit(container){
    await sleep(100+1500*Math.random());
    let bit=Math.trunc(Math.random()*2)
    container.innerText = ''+bit;
  }
  async parse_graphviz(container){
    let text = container.innerText;
    container.innerHTML = 'Loading diagram...';
    let viz = new Viz();
    let elem = await viz.renderSVGElement(text);
    container.replaceWith(elem);
  }
  async parse_d3(container){
    if(container.tagName!='SCRIPT') return;
    assert(container.id!=null, 'Your script must have an id!');
    let f_name = 'd3_'+container.id;
    let f = window[f_name];
    assert(f && container.text.search(f_name)!=-1,
      'Expected function '+f_name+' no found in script of #'+container.id);
    let div = document.createElement('div');
    container.insertAdjacentElement('afterend', div);
    container.classList.forEach(s=>div.classList.add(s))
    let d3_div = d3.select(div);
    return await f(d3_div);
  }

  async parse_collapsible(container){
    let button = document.createElement('span');
    button.innerHTML = '...';
    button.classList.add('collapsible-button');
    button.addEventListener('click', (e)=>{
      container.hidden = !container.hidden;
      button.innerHTML = container.hidden?'...':'(collapse)';
    });
    container.insertAdjacentElement('afterend', button);
    container.hidden = true;
    return;
  }

}

Slides = new SlidesClass();

