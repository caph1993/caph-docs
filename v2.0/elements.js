// sublime-syntax: Lit-Element

caph.makePlugin = ({component, loader=null, post_loader=null})=>{
  return function(){
    const [ready, setReady] = preact.useState(false);
    const [error, setError] = preact.useState(null);
    const _load = async ()=>{
      try{
        await caph.load('caph-docs/core.css');
        if(loader) await loader(...arguments);
        setReady(true);
        if(post_loader) await post_loader(...arguments);
      } catch(err){ setError(err); console.error(err); }
    };
    preact.useEffect(()=>{_load();}, []);
    return html`${
      error? html`<div>Error</div>`
      : (
        ready? component.apply(this, arguments)
        : html`
          <div class="hbox align-center space-around flex"
              style="width:100%;height:100%">
            <div class="plugin-loading"/>
          </div>`
      )
    }`;
  }
}

const CaphSlides = caph.makePlugin({
  component: ({children, theme})=>{
    return html`
    <div class="reveal">
      <div class="slides">
        ${children}
      </div>
    </div>
  `},
  loader: async({theme})=>{
    await caph.load('caph-docs/reveal.js/dist/reveal.js');
    await caph.load('caph-docs/reveal.js/dist/reveal.css');
    if(theme=='marp') caph.load('caph-docs/marp.css');
    else if(theme) caph.load(`caph-docs/reveal.js/dist/theme/${theme}.css`);
    await caph.load('caph-docs/reveal.js/plugin/notes/notes.js');
    await caph.load('caph-docs/reveal.js/plugin/markdown/markdown.js');
    await caph.load('caph-docs/reveal.js/plugin/highlight/highlight.js');
    if(!document.getElementById('theme')){
      await caph.load('caph-docs/reveal.js/dist/theme/simple.css', {
        id: 'theme',
        parent: caph.head_sources,
      });
    }
    if(!document.getElementById('highlight-theme')){
      await caph.load('caph-docs/reveal.js/plugin/highlight/monokai.css',{
        id: 'highlight-theme',
        parent: caph.head_sources,
      });
    }
    await caph.load('caph-docs/slides-core.css',{
      parent: caph.head_sources,
    });
    return;
  },
  post_loader: async({reveal_options})=>{
    let print = (window.location.href.indexOf('?print-pdf')!=-1);
    let options = MyObject.deep_assign({
      progress: true,
      slideNumber: 'c/t',
      history: true,
      //center: true,
      //width: '100%', height: '100%',
      hash: true,
      transition: 'none', // none/fade/slide/convex/concave/zoom
      //plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ],
      keyboard: {
        // Check codes here https://keycode.info/
        //87: ()=> this.set_menu_option('Whiteboard'),// W key
        //27: ()=> this.set_menu_option('Default'),// esc key
      }
    }, reveal_options);
    if(print){
      options.width=options.height='100%';
      center=true;
    }

    await MyPromise.until(()=>
      document.querySelector('.reveal')
      && document.querySelector('.slides')
    );
    Reveal.initialize(options);
    await MyPromise.until(()=>Reveal.isReady());
    if(print) window.print();
    caph.menu.pushOption('PDF print', {
      show:()=>{
        let loc = window.location;
        open(loc.href.slice(0,-loc.hash.length)+'?print-pdf');
        caph.menu.onSelect();
      },
    });
    return;
  }
});

const CaphDocument = caph.makePlugin({
  component: ({children})=>{
    return html`<div class="body hyphenate">${children}</div>`
  },
  loader: async({theme})=>{
    await caph.load('caph-docs/libraries/Hyphenator-5.3.0/Hyphenator.min.js');
    await caph.load('caph-docs/document-core.css');
    caph.menu.pushOption('PDF print', {
      show:()=>{window.print(); caph.menu.onSelect();},
    });
    return;
  },
  post_loader: async({theme})=>{

    return;
  },
});


const MathJaxSvg = caph.makePlugin({
  component: ({children})=>{
    return html`${children}`;
  },
  loader: async()=>{
    window.MathJax = MyObject.deep_assign({
      tex: {
        inlineMath: [['$', '$'], ['\\(', '\\)']],
        macros:{ RR: "{\\mathbb R}", }
      },
      svg: {fontCache: 'local', exFactor: 1.0, scale: 0.9,},
    }, window.MathJax||{});
  },
  post_loader: async()=>{
    if(window.Reveal) await MyPromise.until(()=>Reveal.isReady());
    await caph.load('caph-docs/libraries/mathjax2svg/tex-svg.js', {
      id: 'MathJax-script',
    });
  },
});

