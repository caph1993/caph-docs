

class ResourcesLoader{
  initial_resources = [
    {ref: 'caph-docs/elements/about.js'},
  ];

  _sources = {};
  initial_ready = false;
  
  constructor(){
    this.head_sources = document.getElementById('core-sources');
    if(!this.head_sources){
      let h = document.createElement('div', {id: 'core-sources'});
      document.head.insertAdjacentElement('beforeend', h);
      this.head_sources = h;
    }
    // Packaged resources (if available):
    let attachments = document.getElementById('core-attached-resources');
    this.attached_resources = [];
    if(attachments){
      this.attached_resources = JSON.parse(attachments.innerText);
      document.head.removeChild(attachments);
    }
    this.initialize_resources();
  }
  async initialize_resources(){
    for(let s of this.initial_resources){
      await this.load(s.ref, {
        attrs:{id:s.id, type:s.type||'text/javascript'},
        parent:this.head_sources,
      });
    }
    let load_preact_elems = MyDecorators.once(this.load_preact_elems);
    if(document.readyState=='complete') load_preact_elems();
    window.addEventListener('load', ()=>load_preact_elems());
  }

  async load(ref, {attrs={}, parent=null, where='beforeend',
      auto_attrs=true, require=false}={}){
    // Load an external script or style by inserting relative to parent
    if(parent==null) parent=document.body;
    const ext = ref.split('.').pop();
    let tag = ext=='js'? 'script': ext=='css'? 'link' : null;
    if(tag==null) throw new Error('Only .js and .css files can be _sources. Got: '+ext+' '+ref);
    let defaults = {};
    if(auto_attrs && tag=='link') defaults={rel:'stylesheet', type:'text/css'};
    Object.keys(attrs).forEach(k=>defaults[k]=attrs[k]);
    let content = this.get_attached_resource(ref);
    if(content && tag=='link') tag = 'style';
    attrs = defaults;
    if(content){
      delete attrs.src;
      delete attrs.href;
    } else {
      if(tag=='script') attrs.src = ref;
      if(tag=='link') attrs.href = ref;
    }
    if(require){attrs['data-main']=attrs.src; attrs.src='caph-docs/libraries/require.js-2.3.6/require.js';}
    await this._load_elem(ref, tag, attrs, parent, where, content);
  }

  get_attached_resource(ref){
    for(let e of this.attached_resources){
      if(e.ref==ref) return e.content;
    }
    return null;
  }

  async _load_elem(ref, tag, attrs, parent, where, content){
    // Handle concurrent calls to load_elem(...) about the same ref
    if(!this._sources[ref]){
      this._sources[ref]=1;
      try{
        await this.__load_elem(ref, tag, attrs, parent, where, content);
        this._sources[ref]=2;
      } catch(err){
        this._sources[ref]=0;
        throw err;
      }
    }
    while(this._sources[ref]==1){ // If being loaded in other thread...
      await sleep(80);
    }
  }

  __load_elem(ref, tag, attrs, parent, where, content){
    return new Promise((ok, err)=>{
      let e = document.createElement(tag);
      let done = false;
      e.onload = ()=>(done=true) && ok();
      e.onerror = (x)=>(done=true) && err(x); // HTTP errors only
      Object.keys(attrs).map(key => e.setAttribute(key, attrs[key]));
      if(content){
        let r = window._loaded_resources||{};
        window._loaded_resources = r;
        r[ref] = false;
        content += `\nwindow._loaded_resources['${ref}']=true;\n`
        e.innerHTML = content;
        if(tag=='script'){
          (async()=>{
            while(!r[ref]) await new Promise((o,e)=>setTimeout(o,100));
            done=true; ok();
          })();
        } else if(tag=='style'){
          let ms = 10;
          setTimeout(()=>(done=true) && ok(), ms);
        }
      }
      parent.insertAdjacentElement(where, e);
      setTimeout(()=>done||err(['Timeout (3s) loading source:', e]), 3000);
    });
  };
}

var caph = new ResourcesLoader(); // Load core_resources
