
class PluginFabricEditor{
  constructor(){
    this.main = document.createElement('div');
    this.main.classList.add('fabric-editor-main', 'vbox');
    this.loaded = {};
    this.hidden = true;
  }
  get hidden(){ return this._hidden; }
  set hidden(hidden){
    if(hidden) this.main.classList.add('hidden');
    else this.main.classList.remove('hidden');
    this._hidden = hidden;
  }

  async init(Slides, container){
    if(this.already) return;
    this.already=true;
    container.appendChild(this.main);

    let header = MyDocument.createElement('div', {
      classList: ['fabric-editor-header', 'hbox'],
      style: {height: '2rem'},
      parent: this.main,
    });
    let full_body = MyDocument.createElement('div', {
      classList: ['flex', 'vbox'],
      style: {maxHeight: 'calc(100% - 2rem)'},
      parent: this.main,
    });
    let body = MyDocument.createElement('div', {
      classList: ['fabric-editor-body', 'vbox'],
      parent: full_body,
    });
    let footer = MyDocument.createElement('div', {
      classList: ['fabric-editor-footer', 'hbox'],
      parent: full_body,
    });
    Split(full_body.children, {direction: 'vertical'});

    let body_cm = MyDocument.createElement('div', {
      classList: ['flex', 'fabric-editor-code'],
      parent: body,
    });
    let cm_options={
      theme: 'monokai',
      indentUnit: 2,
      tabSize: 2,
      lineWrapping: true,
      lineNumbers: true,
      keyMap: 'sublime',
      scrollPastEnd: true,
    }
    this.cm = CodeMirror(body_cm, {
      value: this.template(),
      mode:  'javascript',
      extraKeys:{
        'Ctrl-Enter': (cm) => this.run(),
      },
      ...cm_options,
    });

    this.canvas_div = MyDocument.createElement('div', {
      classList: ['flex' ,'vbox'],
      parent: footer,
    });

    let footer_cm = MyDocument.createElement('div',{
      classList: ['flex', 'fabric-editor-code'],
      parent: footer,
    });
    this.cm_footer = CodeMirror(footer_cm, {
      value: 'Select an element to see its properties...',
      mode:  'json',
      ...cm_options,
    });

    MyDocument.createElement('button', {
      text: 'Run (ctrl+enter)',
      eventListeners:{
        click: ()=>this.run()
      },
      parent: header,
    });
    MyDocument.createElement('span', {
      text: 'Scale figure:',
      parent: header,
    });

    MyDocument.createElement('input', {
      type: 'range', value: '100', min: '10', max: '100',
      parent: header,
      eventListeners: {
        change: (e)=>{
          let value = e.srcElement.value/100;
          this.canvas_div.style.transform = `scale(${value})`;
          //this.canvas_div.style['transform-origin']='50% 50%';
        },
      },
    });

    Slides.add_menu_option('Figure editor', {
      show: ()=>{
        this.hidden=false;
        this.cm.refresh();
        this.cm_footer.refresh();
        this.run();
      },
      hide: ()=>(this.hidden=true),
    });
  }
  template(){
    let tmp = `
    (canvas)=>{
      let elems = [
        new fabric.Ellipse({
        left: 10, top: 10, fill: 'red', stroke: 'black',
        opacity: 0.5, rx: 150, ry: 80,
        }),
          new fabric.Ellipse({
          left: 70, top: 70, fill: 'green', stroke: 'black',
          opacity: 0.5, rx: 150, ry: 80,
        }),
        new fabric.Text('Truth', { left: 103, top: 24 }),
        new fabric.Text('Knowledge', { left: 90, top: 100 }),
        new fabric.Text('Belief', { left: 200, top: 180 }),
      ];
      elems.forEach(e=>canvas.add(e));
      canvas.setHeight(280);
      canvas.setWidth(400);
    }`;
    let noindent = tmp.split('\n').map(
      s=>s.slice(4,s.length)
    ).join('\n').trim();
    return noindent+'\n';
  }
  async run(){
    this.canvas_div.innerHTML = '';
    let canvas = MyDocument.createElement('canvas', {
      parent: this.canvas_div,
    });
    let code = this.cm.getValue();
    let fabric_canvas = new fabric.Canvas(canvas);
    fabric_canvas.on('object:modified', (ev)=>this.on_canvas_event(ev));
    //fabric_canvas.on('selection:created', (ev)=>this.on_canvas_event(ev));
    fabric_canvas.on('selection:updated', (ev)=>this.on_canvas_event(ev));
    this.cm_footer.setValue('');
    try{ await eval(code)(fabric_canvas); }
    catch(err){ this.cm_footer.setValue(''+err); }
  }
  on_canvas_event(ev){
    let obj = ev.target;
    let s = JSON.stringify(obj, null, '  ');
    this.cm_footer.setValue(s);
  }
}

var pluginFabricEditor = new PluginFabricEditor();