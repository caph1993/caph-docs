
class PluginCodemirror{

  async parse(Slides, container){
    let code = container.innerText.trim();
    container.innerHTML='';
    container.classList.add('codemirror-container');
    let cm_options={
      theme: 'monokai',
      indentUnit: 2,
      tabSize: 2,
      lineWrapping: true,
      lineNumbers: true,
      keyMap: 'sublime',
      scrollPastEnd: false,
      autoRefresh: true, // necessary for Reveal.js
    }
    const cm = CodeMirror(container, {
      value: code,
      mode: 'python',
      ...cm_options,
    });
  }
}

var pluginCodemirror = new PluginCodemirror();