let fs = require('fs');

let baseDir = 'caph-docs/';

let sources = [
  // order matters!
  "core/utils.js",
  "core/htm-math.js",
  "core/htm-preact.js",
  "core/dynamic-loader.js",
];
let dynamicSources = [
  "core/core.css",
  "core/document.css",
  "core/marp.css",
  "core/menu.js",
  "core/elements.js",
  "core/fonts.css",
  "core/katex.min.js",
  "core/katex-nofonts.min.css",
  "core/reveal.js",
  "core/reveal.css",
  "core/hyphenator.min.js",
];


async function main(){
  let buff = '';
  for(let path of sources){
    buff+= fs.readFileSync(path);
  }
  let attachments = [];
  for(let path of dynamicSources){
    attachments.push({
      ref: baseDir+path,
      content: ''+fs.readFileSync(path),
    });
  }

  fs.writeFileSync('dist/caph-docs.js', `
window.caph_attachments=(window.caph_attachments||[]).concat(${JSON.stringify(attachments)});
${buff}
delete window.caph_attachments;
`);
}
main();