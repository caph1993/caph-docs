from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import os, json, base64, re, sys
import xml.etree.ElementTree as ET

_, file, file_out = sys.argv
assert os.path.abspath(file)!=os.path.abspath(file_out), "Don't do that buddy!"

file = os.path.abspath(file)
file = {
    'abspath': file,
    'dir': os.path.dirname(file),
    'url': 'file://'+file,
    'url_dir': 'file://'+os.path.dirname(file),
    'content': open(file).read(),
}

def find_core(html):
    reg = r'< *script.*?>.*?</script>'
    scripts = re.findall(reg, html, re.DOTALL)
    for s in scripts:
        t = ET.fromstring(s)
        src = t.attrib.get('src', '')
        if src.endswith('caph-docs/core.js'):
            return {'literal': s, 'attrs':t.attrib}
    return None


core = find_core(file['content'])
assert core, 'core.js was not found in input file'


driver = webdriver.Chrome(ChromeDriverManager().install())

print('Waiting for document to load')

driver.get(file['url'])

try:
    driver.execute_async_script("""
    while(!caphDocument || !caphDocument._ready){
        await new Promise((ok,_)=>setTimeout(ok, 100));
        console.log('waiting...');
    }
    console.log('ready!');
    throw new Error(); // Hotfix, driver was not leaving the script
    """)
except:
    pass

def hsize(n):
    units = ['GB', 'MB', 'kB', 'B ']
    while n>1000: units.pop(); n /= 1000
    return f'{round(n):3d} {units.pop()}'

print('Extracting sources')

sources = [
    *[e.get_attribute('src') for e in driver.find_elements_by_tag_name("script")],
    *[e.get_attribute('href') for e in driver.find_elements_by_tag_name("link")],
]
sources = [{'url': s, 'ref':s[len(file['url_dir'])+1:]} for s in sources]
sources = [{**s, 'abspath': os.path.join(file['dir'], s['ref'])} for s in sources if s['ref']]
sources = [{'ref': s['ref'], 'content': open(s['abspath']).read()} for s in sources]
sources = [{**s, 'size': len(s['content'])} for s in sources]
sources = [{**s, 'hsize': hsize(s['size'])} for s in sources]


print('Extracting images')

images = [
    {'ref': e.get_attribute('src'), 'html':e.get_attribute('outerHTML')}
    for e in driver.find_elements_by_tag_name("img")
]


exts = {
    'png': 'png',
    'jpg': 'jpeg',
    'jpeg': 'jpeg',
    'gif': 'gif',
}
for img in images:
    img['new_html'] = img['html']
    img['base64'] = ''
    if img['ref'].startswith('file://'):
        img['abspath'] = img['ref'][7:]
    elif img['ref'].startswith('data:image'):
        continue
    else:
        print("SKIPPING external image (not yet supported)")
        print(img['ref'])
        continue
    img['bcontent'] = open(img['abspath'], 'rb').read()
    img['base64'] = base64.b64encode(img['bcontent']).decode()
    img['ext'] = next(v for k,v in exts.items() if img['ref'].endswith(k))
    img['serialized'] = "data:image/{ext};base64,{base64}".format(**img)
    raw_src = [
        *re.findall(r'src=\'(.*?)\'', img['html']),
        *re.findall(r'src=\"(.*?)\"', img['html']),
    ][0]
    img['new_html'] = img['html'].replace(raw_src, img['serialized'])

for img in images:
    img['size'] = len(img['base64'])
    img['hsize'] = hsize(img['size'])


print('Extracting fonts')

exts = {
    'ttf': 'truetype',
    'woff': 'woff',
    'woff2': 'woff2',
    'otf': 'otf',
}
fonts = []
for s in sources:
    if s['ref'].endswith('.css'):
        for x in re.findall(r'url *\(.*?\)', s['content']):
            sdir = os.path.join(file['dir'], os.path.dirname(s['ref']))
            y = re.findall(r'url *\( *(.*?) *\)', x)[0]
            font = {'ref': y, 'abspath': os.path.join(sdir, y)}
            if font['ref'].startswith('data:'):
                continue
            if not os.path.exists(font['abspath']):
                continue
            font['bcontent'] = open(font['abspath'], 'rb').read()
            font['base64'] = base64.b64encode(font['bcontent']).decode()
            font['ext'] = next(v for k,v in exts.items() if font['ref'].endswith(k))
            font['serialized'] = "url(data:font/ttf;base64,{base64}) format('{ext}');".format(**font)
            s['content'] = s['content'].replace(x, font['serialized'])
            font['size'] = len(font['base64'])
            font['hsize'] = hsize(font['size'])
            fonts.append(font)

for s in sources:
    print('  {hsize} {ref}'.format(**s))
for s in fonts:
    print('  {hsize} {ref}'.format(**s))
for s in images:
    print('  {hsize} {ref}'.format(**s))

driver.close()

core_js = next(s['content'] for s in sources
    if s['ref']=='caph-docs/core.js')
core_attrs = ' '.join(f'{k}="{v}"' for k,v in core['attrs'].items() if k!='src')

html = file['content'].replace(core['literal'], f'''
<script id="core-attached-resources">
{json.dumps(sources)}
</script>
<script {core_attrs}>
{core_js}
</script>
''')

# Replace images!
for img in images:
    html = html.replace(img['html'], img['new_html'])

with open(file_out, 'w') as f:
    f.write(html)

