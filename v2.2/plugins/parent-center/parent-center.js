
class PluginParentCenter{

  async parse(container){
    let sibling = MyDocument.createElement('div', {
      parent: container, where: 'afterend',
      classList: ['hbox', 'boxcenter'],
    });
    sibling.appendChild(container);
  }
}

var pluginParentCenter = new PluginParentCenter();
