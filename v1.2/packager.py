from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import os, json, base64, re

driver = webdriver.Chrome(ChromeDriverManager().install())

file = "tests/email.html"
file_out = "out.html"
file = os.path.abspath(file)
file = {
    'abspath': file,
    'dir': os.path.dirname(file),
    'url': 'file://'+file,
    'url_dir': 'file://'+os.path.dirname(file),
    'content': open(file).read(),
}

print('Waiting for document to load')

driver.get(file['url'])

try:
    driver.execute_async_script("""
    while(!window.CaphMain || !CaphMain._ready){
        await new Promise((ok,_)=>setTimeout(ok, 100));
        console.log('waiting...');
    }
    console.log('ready!');
    throw new Error(); // Hotfix, driver was not leaving the script
    """)
except:
    pass

print('Extracting sources')

sources = [
    *[e.get_attribute('src') for e in driver.find_elements_by_tag_name("script")],
    *[e.get_attribute('href') for e in driver.find_elements_by_tag_name("link")],
]
sources = [{'url': s, 'ref':s[len(file['url_dir'])+1:]} for s in sources]
sources = [{**s, 'abspath': os.path.join(file['dir'], s['ref'])} for s in sources if s['ref']]
sources = [{'ref': s['ref'], 'content': open(s['abspath']).read()} for s in sources]

exts = {
    'ttf': 'truetype',
    'woff': 'woff',
    'woff2': 'woff2',
    'otf': 'otf',
}
fonts = []
for s in sources:
    if s['ref'].endswith('.css'):
        for x in re.findall(r'url *\(.*?\)', s['content']):
            sdir = os.path.join(file['dir'], os.path.dirname(s['ref']))
            y = re.findall(r'url *\( *(.*?) *\)', x)[0]
            font = {'ref': y, 'abspath': os.path.join(sdir, y)}
            print(font)
            if font['ref'].startswith('data:'):
                continue
            if not os.path.exists(font['abspath']):
                continue
            font['bcontent'] = open(font['abspath'], 'rb').read()
            font['base64'] = base64.b64encode(font['bcontent'])
            font['ext'] = next(v for k,v in exts.items() if font['ref'].endswith(k))
            font['serialized'] = "url(data:font/ttf;base64,{base64}) format('{ext}');".format(**font)
            s['content'] = s['content'].replace(x, font['serialized'])
            fonts.append(font)

for s in sources:
    print('  {ref}'.format(**s))
for s in fonts:
    print('  {ref}'.format(**s))

core = next(s['content'] for s in sources if s['ref']=='caph-docs/core.js')
json_sources = json.dumps(sources)
full_script = f'var caphStaticResources = {json_sources};\n\n'+core

full_script = f'<script id="document-core">\n{full_script}\n</script>'
new_content = file['content'].replace('<script src="caph-docs/core.js" id="document-core"></script>', full_script)

open(file_out, 'w').write(new_content)

driver.close()
