
function assert(condition, ...messages){
  if(condition) return;
  throw new Error(messages);
}

function sleep(ms){
  return new Promise((ok,err)=>setTimeout(ok, ms));
}

class MyObject{

  static indexBy(arr, key){
    let M = {};
    arr.forEach((obj)=>{
      let value = obj[key];
      M[value] = obj;
    });
    return M;
  }

  /**
  @param {((value:any, key?:any, obj?:any)=>(any))} func
  */
  static map(obj, func){
    return MyObject._object_op('map', obj, func);
  }

  /**
  @param {((value:any, key?:any, obj?:any)=>(any))} func
  */
  static filter(obj, func){
    return MyObject._object_op('filter', obj, func);
  }

  /**
  @param {((value:any, key?:any, obj?:any)=>(any))} func
  */
  static apply(obj, func){
    return MyObject._object_op('apply', obj, func);
  }

  /**
  @param {((value:any, key?:any, obj?:any)=>(any))} func
  */
  static forEach(obj, func){
    return MyObject._object_op('forEach', obj, func);
  }

  static _object_op(op, src, func){
    assert(1<=func.length && func.length<=3);
    let dest = {};
    for(let key of Object.keys(src)){
      let value = src[key]
      let result = func.length==1? func(value) : func.length==2?
        func(value,key) : func(value, key, src);
      if(op=='map') dest[key] = result;
      else if(op=='filter'){ if(result) dest[key] = value; }
      else if(op=='apply') src[key] = result;
      else if(op=='forEach') 1==1;
    }
    return (op=='map'||op=='filter')? dest : null;
  }

  static reduce_dots(obj){
    // converts {'a.b':1, 'a.c':2, b:3} into {a:{b:1,c:2}, 'b':3}. Returns copy (shallow or deep)
    let dotted = MyObject.object_filter(obj, (v,k)=>k.indexOf('.')>=0);
    let no_dots = MyObject.object_filter(obj, (v,k)=>k.indexOf('.')==-1);
    if(Object.keys(dotted).length==0) return no_dots;
    MyObject.object_forEach(dotted, (v,k)=>{
      let start = k.slice(0, k.indexOf('.'));
      let end = k.slice(k.indexOf('.')+1);
      no_dots[start] = no_dots[start]||{};
      no_dots[start][end] = v;
    });
    return MyObject.reduce_dots(no_dots);
  }

  static deep_copy(obj){
    if(obj===undefined) return undefined;
    if(obj===null) return null;
    if(Array.isArray(obj)) return obj.map(x=>deep_copy(x));
    if(typeof(obj)=="object") return MyObject.object_map(obj, value=>MyObject.deep_copy(value));
    if(typeof(obj)=="function") return obj; // DOES NOT CREATE COPY FOR FUNCTIONS
    return obj;
  }

  static deep_assign(obj, ...objs){
    for(let o of objs){
      for(let key of Object.keys(o)){
        if(obj[key]===undefined || typeof(o[key])!="object") obj[key] = o[key];
        else MyObject.deep_assign(obj[key], o[key]);
      }
    }
    return obj;
  }
}

function all_true(arr){
  for(let x of arr) if(!x) return false;
  return true;
}
function any_true(arr){
  for(let x of arr) if(x) return true;
  return false;
}
function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;
  for(let i=0; i<a.length; ++i){
    if (a[i] !== b[i]) return false;
  }
  return true;
}

class MyPromise{
  static async finish_all(promises, {debug=false}={}){
    // Ensures completion of all promises even if some throw exceptions
    return await new Promise((ok, err)=>{
      if(promises.length==0) return ok([]);
      let any_error=false;
      let cnt = 0;
      let outs = promises.map((p)=>null);
      Promise.all(promises.map(async (p, i)=>{
        try{ outs[i] = await p; }
        catch(e){
          any_error=true; outs[i] = e;
          if(debug) console.error(...e);
        }
        cnt++;
        if(cnt==promises.length){
          if(any_error) err(outs);
          else ok(outs);
        }
      }));
    });
  }
  static async finish_all_log(promises){
    try{ var outs = await MyPromise.finish_all(promises); }
    catch(err){ var outs = err; console.error(...err); }
    return outs;
  }
  static async timeout(promise, ms){
    let [resp, _] = await Promise.all([
      promise,
      (async ()=>{ await sleep(ms); throw 'Timeout'; })(),
    ]);
    return resp;
  }
}

function get_property_handler(object, property){
  let access, proto = object;
  object._hidden_modified = false;
  while(!access){
    proto = Object.getPrototypeOf(proto);
    access = Object.getOwnPropertyDescriptor(proto, property);
  }
  return access;
}

function update_property_handler(object, property, create_handler){
  let prev = get_property_handler(object, property);
  Object.defineProperty(object, property, create_handler(prev));
}


class MyDocument{
  static createElement(tag, {
      style={}, id={}, classList=[], text=null, html=null,
      eventListeners={}, parent=null, where=null, ...attrs}={}){
    let e = document.createElement(tag, id?{id:id}:null);
    classList.forEach(s=>e.classList.add(s));
    if(text!=null) e.innerText = text;
    if(html!=null) e.innerHTML = html;
    MyObject.forEach(attrs, (value, key)=>e.setAttribute(key, value));
    MyObject.forEach(style, (value, key)=>e.style[key] = value);
    MyObject.forEach(eventListeners, (value, key)=>
      e.addEventListener(key, value));
    if(parent || where){
      parent = parent||document.body;
      where = where||'beforeend';
      parent.insertAdjacentElement(where, e);
    }
    return e;
  }
  static right_click(element){
    element.focus();
    if (window.CustomEvent) {
      element.dispatchEvent(new CustomEvent('contextmenu'));
    } else if (document.createEvent) {
      var ev = document.createEvent('HTMLEvents');
      ev.initEvent('contextmenu', true, false);
      element.dispatchEvent(ev);
    } else { // Internet Explorer
      element.fireEvent('oncontextmenu');
    }
    return;
    element.focus();
    let e = element.ownerDocument.createEvent('MouseEvents');
    e.initMouseEvent('contextmenu', true, true,
      element.ownerDocument.defaultView, 1, 0, 0, 0, 0, false,
      false, false, false, 2, null);
    return !element.dispatchEvent(e);
  }
}

class DocsCore{
  _sources = {}
  _already_loaded = false
  _ready = false

  constructor(script_element){
    if(document.getElementById('core-sources')) return; // Loaded
    this.head_sources = MyDocument.createElement('div', {
      parent: script_element,
      where: 'afterend',
      id: 'core-sources',
    });
    window.addEventListener('load', ()=>this._on_window_load());
    window.addEventListener('keydown', (e)=>{
      if (e.keyCode == 116) this.save_scroll_position(); // F5 key
    });
  }
  save_scroll_position(){
    window.localStorage.setItem('scrollX', ''+window.scrollX);
    window.localStorage.setItem('scrollY', ''+window.scrollY);
    if(document && document.body)
      window.localStorage.setItem('scrollHeight', ''+document.body.scrollHeight);
  }
  load_scroll_position(force){
    let scrollX = parseInt(window.localStorage.getItem('scrollX'));
    let scrollY = parseInt(window.localStorage.getItem('scrollY'));
    let scrollHeight = parseInt(window.localStorage.getItem('scrollHeight'));
    if(force || document.body.scrollHeight!=scrollHeight)
      window.scrollBy(scrollX-window.scrollX, scrollY-window.scrollY);
  }
  load_href_scroll_position(){
    let href =  window.location.href;
    if (href.indexOf('startAt') != -1 ) {
      let match = href.split('?')[1].split("&")[0].split("=");
      console.log(match);
      document.getElementsByTagName("body")[0].scrollTop = match[1];
    }
  }
  async _on_window_load(){
    if(this._already_loaded) return;
    this._already_loaded = true;
    // restore previous scroll position if available
    this.init_menu();
    let scroller = setInterval(this.load_scroll_position, 50000);
    try{
      await this.on_window_load();
    } finally{
      clearInterval(scroller);
      this.load_scroll_position();
      this._ready = true;
    }
  }
  async on_window_load(){ throw new Error('Abstract method')}

  async load(ref, {attrs={}, parent=null, where='beforeend',
      auto_attrs=true}={}){
    // Load an external script or style by inserting relative to parent
    if(parent==null) parent=document.body;
    const ext = ref.split('.').pop();
    let tag = ext=='js'? 'script': ext=='css'? 'link' : null;
    let content = this.get_static_resource(ref);
    if(content && tag=='link') tag = 'style';
    assert(tag!=null, 'Only .js and .css files can be _sources. Got: '+ext+' '+ref);
    let defaults = {};
    if(auto_attrs && tag=='link') defaults={rel:'stylesheet', type:'text/css'};
    MyObject.deep_assign(defaults, attrs);
    attrs = defaults;
    if(content){
      delete attrs.src;
      delete attrs.href;
    } else {
      if(tag=='script') attrs.src = ref;
      if(tag=='link') attrs.href = ref;
    }
    await this._load_elem(ref, tag, attrs, parent, where, content);
  }

  async _load_elem(ref, tag, attrs, parent, where, content){
    // Handle concurrent calls to load_elem(...) about the same ref
    if(!this._sources[ref]){
      this._sources[ref]=1;
      try{
        await this.__load_elem(ref, tag, attrs, parent, where, content);
        this._sources[ref]=2;
      } catch(err){
        this._sources[ref]=0;
        throw err;
      }
    }
    while(this._sources[ref]==1){ // If being loaded in other thread...
      await sleep(1500);
    }
  }

  __load_elem(ref, tag, attrs, parent, where, content){
    return new Promise((ok, err)=>{
      let e = document.createElement(tag);
      let done = false;
      Object.keys(attrs).map(key => e.setAttribute(key, attrs[key]));
      if(content){
        let id = ''+Math.random();
        window._loaded_resources = (window._loaded_resources||{})
        window._loaded_resources[ref] = false;
        content += `\nwindow._loaded_resources['${ref}']=true;\n`
        e.innerHTML = content;
        if(tag=='script'){
          (async()=>{
            while(!window._loaded_resources[ref]) await sleep(100);
            done=true; ok();
          })();
        } else if(tag=='style'){
          let ms = 500;
          setTimeout(()=>(done=true) && ok(), ms);
        }
      } else{
        e.onload = ()=>(done=true) && ok();
        e.onerror = (x)=>(done=true) && err(x); // HTTP errors only
      }
      parent.insertAdjacentElement(where, e);
      setTimeout(()=>done||err(['Timeout (3s) loading source:', e]), 3000);
    });
  };

  get_static_resource(ref){
    for(let e of (window.caphStaticResources||[])){
      if(e.ref==ref) return e.content;
    }
    return null;
  }

  get_elems(css_class){
    return [...document.getElementsByClassName(css_class)];
  }

  async load_plugins(){
    // Filter used plugins and sort them in priority groups 
    let plugins = {};
    this.plugins.forEach((p)=>{
      if(!this.get_elems(p.css_class).length) return;
      let key = p.priority||0;
      plugins[key] = (plugins[key]||[]);
      plugins[key].push(p);
    });
    plugins = Object.keys(plugins).sort().reverse().map(k=>plugins[k]);
    
    for(let group of plugins){
      let promises = group.map(p=>this.load_plugin(p));
      await MyPromise.finish_all_log(promises);
    }
  }

  async load_plugin(plugin){
    // Get all elements of the class
    const css_class = plugin.css_class;
    const scripts = plugin.scripts || [];
    const styles = plugin.styles || [];
    const parser = plugin.parser || ((_)=>
      console.log('No parser for', css_class)
    );
    
    // Create loaders in DOM
    const css_loader = 'parser-loading tmp-'+css_class;
    const src_elements = this.get_elems(css_class);
    if(!src_elements.length) return;

    const was_hidden = src_elements.map(e=>e.hidden);
    let all_diff = (a,b) => all_true(a.map((_,i)=>a[i]!=b[i]));
    await Promise.all(src_elements.map(async e =>{
      let loader = document.createElement('div');
      css_loader.split(' ').map(s=>loader.classList.add(s));
      e.insertAdjacentElement('afterend', loader);
      e.hidden = true;
      return;
    }));
    
    // Wait for elements query to update (this is weird but necessary)
    const ready = () => all_diff(src_elements, this.get_elems(css_class));
    try{
      let wait = async ()=>{ while(!ready()) await sleep(5); };
      await MyPromise.timeout(wait(), 1500);
    } catch(e){ if(e!='Timeout') throw e; }

    const elements = this.get_elems(css_class);

    // Define restore_hidden_state() for each element
    elements.forEach((e,i)=>{
      e._hidden_modified = false;
      e._was_hidden = was_hidden[i];
      if(!e._hidden_redefined){
        update_property_handler(e, 'hidden', (prev)=>({
          get: function () {
            return prev.get.call(this);
          },
          set: function (val) {
            this._hidden_modified = true;
            return prev.set.call(this, val);
          }
        }));
        e.restore_hidden_state = function(){
          if(!this._hidden_modified) this.hidden=this._was_hidden;
        }
      }
      e._hidden_redefined = true;
    });

    try{
      // Import scripts and styles in series (dependency order)
      if(elements.length){
        for(let src of styles){await this.load(src)};
        for(let src of scripts){await this.load(src)};
      }
      // Run parser functions and clear loaders
      await MyPromise.finish_all(elements.map(async (e) =>{
        try{
          await parser(e);
        } finally {
          e.restore_hidden_state();
          this.get_elems(css_loader).forEach(x=>x==e.nextSibling?x.remove():null);
        }
      }));
    } finally{
      // Remove possible remaining loaders and unhide possible hidden 
      // This may happen because of the 'liveness' of this.get_elems
      this.get_elems(css_loader).forEach(e=>e.remove());
    }
  }

  plugins = [
    // Scripts are not loaded if not used :)
    // High priority plugins are loaded first. default: 0
    {
      css_class: "random-bit",
      scripts: [],
      styles: [],
      parser: async (c)=>(await this.parse_random_bit(c)),
    },
    {
      css_class: "graphviz",
      parser: async (c)=>(await this.parse_graphviz(c)),
      scripts: [
        'caph-docs/libraries/viz/viz.js',
        'caph-docs/libraries/viz/full.render.js',
      ],
    },
    {
      css_class: "fabric",
      parser: async (c)=>(await pluginFabric.parse(c)),
      scripts: [
        'caph-docs/libraries/mathjax2svg/tex-svg.js',
        'caph-docs/libraries/fabric/fabric.js',
        'caph-docs/plugins/fabric/fabric.js',
      ],
    },
    {
      css_class: "d3",
      parser: async (c)=>(await this.parse_d3(c)),
      scripts: [
        'caph-docs/libraries/d3/d3.v5.min.js',
      ],
    },
    {
      css_class: "fabric-whiteboard",
      parser: async (c)=>(await pluginWhiteboard.init(this, c)),
      scripts: [
        'caph-docs/libraries/fabric/fabric.js',
        'caph-docs/plugins/fabric-whiteboard/whiteboard.js',
      ],
      styles: [
        'caph-docs/plugins/fabric-whiteboard/whiteboard.css',
      ],
      priority: -1,
    },
    {
      css_class: "fabric-editor",
      parser: async (c)=>(await pluginFabricEditor.init(this, c)),
      scripts: [
        'caph-docs/libraries/split/split.js',
        'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.js',
        'caph-docs/libraries/codemirror-5.55.0/mode/javascript/javascript.js',
        'caph-docs/libraries/codemirror-5.55.0/addon/search/searchcursor.js',
        'caph-docs/libraries/codemirror-5.55.0/addon/search/search.js',
        'caph-docs/libraries/codemirror-5.55.0/addon/scroll/scrollpastend.js',
        'caph-docs/libraries/codemirror-5.55.0/keymap/sublime.js',
        'caph-docs/libraries/fabric/fabric.js',
        'caph-docs/plugins/fabric-editor/fabric-editor.js',
      ],
      styles: [
        'caph-docs/libraries/split/split.css',
        'caph-docs/libraries/codemirror-5.55.0/lib/codemirror.css',
        'caph-docs/libraries/codemirror-5.55.0/theme/monokai.css',
        'caph-docs/plugins/fabric-editor/fabric-editor.css',
      ],
      priority: -1,
    },
    {
      css_class: "blockquote",
      parser: (c)=>null,
      styles: [
        'caph-docs/plugins/blockquote/blockquote.css',
      ],
      priority: 9,
    },
    {
      css_class: "parent-center",
      parser: async (c)=>(await pluginParentCenter.parse(c)),
      scripts: [
        'caph-docs/plugins/parent-center/parent-center.js',
      ],
      priority: 10,
    },
    {
      css_class: "collapsible",
      parser: async (c)=>(await this.parse_collapsible(c)),
      priority: 9,
    }
  ]


  init_menu(){
    this.menu = document.createElement('select', {id:'slides-menu'});
    this.menu.classList.add('slides-menu');
    document.body.appendChild(this.menu);
    this.menu_options = [];
    this.menu.addEventListener('change', (e)=>{
      let selected = this.menu.value;
      this.menu_options.forEach(({text, show, hide})=>{
        if(text==selected) show();
        else hide();
      });
      if(selected=='Default') this.menu.classList.remove('slides-menu-shown');
      else this.menu.classList.add('slides-menu-shown');
    });
    this.add_menu_option('Default');
    this.add_menu_option('Pack', {show:()=>{
      let scripts = document.getElementsByTagName('script');
      console.log(scripts);
    }});
  }

  add_menu_option(text, {show=()=>null, hide=()=>null}={}){
    this.menu_options.push({text, show, hide});
    let opt = document.createElement('option');
    opt.innerText = text;
    this.menu.insertAdjacentElement('beforeend', opt);
    if(this.menu_options.length>1){
      this.menu.classList.add('slides-menu-multiple');
    }
  }
  async parse_random_bit(container){
    await sleep(100+1500*Math.random());
    let bit=Math.trunc(Math.random()*2)
    container.innerText = ''+bit;
  }
  async parse_graphviz(container){
    let text = container.innerText;
    container.innerHTML = 'Loading diagram...';
    let viz = new Viz();
    let elem = await viz.renderSVGElement(text);
    container.replaceWith(elem);
  }
  async parse_d3(container){
    if(container.tagName!='SCRIPT') return;
    assert(container.id!=null, 'Your script must have an id!');
    let f_name = 'd3_'+container.id;
    let f = window[f_name];
    assert(f && container.text.search(f_name)!=-1,
      'Expected function '+f_name+' no found in script of #'+container.id);
    let div = document.createElement('div');
    container.insertAdjacentElement('afterend', div);
    container.classList.forEach(s=>div.classList.add(s))
    let d3_div = d3.select(div);
    return await f(d3_div);
  }

  async parse_collapsible(container){
    let button = document.createElement('span');
    button.innerHTML = '...';
    button.classList.add('collapsible-button');
    button.addEventListener('click', (e)=>{
      container.hidden = !container.hidden;
      button.innerHTML = container.hidden?'...':'(collapse)';
    });
    container.insertAdjacentElement('afterend', button);
    container.hidden = true;
    return;
  }
}

class CaphSlides extends DocsCore{

  constructor(script_element){
    super(script_element);
    this.load('caph-docs/reveal.js/dist/reset.css', {parent:this.head_sources});
    this.load('caph-docs/reveal.js/dist/reveal.css', {parent:this.head_sources});
  }

  async on_window_load(){
    await this.load('caph-docs/reveal.js/dist/reveal.js');
    let on_ready = async()=>{
      try{ await this.load_plugins(); }
      finally{ Reveal.sync(); }
    }
    if(Reveal.isReady()) on_ready();
    else Reveal.on('ready', (e)=> on_ready());
    await this.init();
  }

  async init(){
    await this.load('caph-docs/reveal.js/plugin/notes/notes.js');
    await this.load('caph-docs/reveal.js/plugin/markdown/markdown.js');
    await this.load('caph-docs/reveal.js/plugin/highlight/highlight.js');

    if(!document.getElementById('theme')){
      await this.load('caph-docs/reveal.js/dist/theme/simple.css', {
        id: 'theme',
        parent: this.head_sources,
      });
    }
    if(!document.getElementById('highlight-theme')){
      await this.load('caph-docs/reveal.js/plugin/highlight/monokai.css',{
        id: 'highlight-theme',
        parent: this.head_sources,
      });
    }
    await this.load('caph-docs/core.css', {
      parent: this.head_sources,
    });
    await this.load('caph-docs/slides-core.css',{
      parent: this.head_sources,
    });

    Reveal.initialize({
      progress: true,
      slideNumber: 'c/t',
      history: true,
      center: true,
      //width: '100%', height: '100%', hash: true,
      transition: 'none', // none/fade/slide/convex/concave/zoom
      plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ],
      dependencies: [
          {
            src: 'caph-docs/libraries/reveal.js-math-katex-plugin/math-katex.js',
            async: true,
          },
          //{ src: 'caph-docs/libraries/reveal.js-rajgoel-plugins/chalkboard/chalkboard.js' },
      ],
      math:{
        katexScript:     'caph-docs/libraries/katex/katex.min.js',
        katexStylesheet: 'caph-docs/libraries/katex/katex.min.css'
      },
      keyboard: {
        // Check codes here https://keycode.info/
         8: ()=> console.log('key pressed'),
      },
    });
  }
}


class CaphDocument extends DocsCore{

  async on_window_load(){
    await this.load('caph-docs/libraries/katex/katex.min.js', {
      parent: this.head_sources,
    });
    await this.load('caph-docs/libraries/katex/katex.min.css', {
      parent: this.head_sources,
    });
    await this.load('caph-docs/libraries/katex/contrib/auto-render.min.js', {
      parent: this.head_sources,
    });
    renderMathInElement(document.body, {
      delimiters: [
        {left: "$", right: "$", display: false},
        {left: "$$", right: "$$", display: true},
        {left: "\\(", right: "\\)", display: false},
        {left: "\\[", right: "\\]", display: true}
      ]
    });

    await this.load('caph-docs/libraries/Hyphenator-5.3.0/Hyphenator.min.js', {
      parent: this.head_sources,
    });
    await this.load('caph-docs/core.css', {
      parent: this.head_sources,
    });
    await this.load('caph-docs/document-core.css',{
      parent: this.head_sources,
    });
    await this.load_plugins();
  }
}


class CaphStarter{
  explain = 'Use either <script id="slides-core" src="..."> or <script id="document-core" src="...">';
  constructor(){
    let slides = document.getElementById('slides-core');
    let doc = document.getElementById('document-core');
    let err;
    if(!slides && !doc) err='Mode was not specified';
    if(slides && doc) err='Conflicting modes specified';
    assert(!err, err+'. '+this.explain);
    this.main = slides? new CaphSlides(slides) : new CaphDocument(doc);
  }
}

var CaphMain = (new CaphStarter()).main;
