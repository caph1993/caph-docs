class PluginFabric{

  async parse(container){
    if(container.tagName!='SCRIPT') return;
    let canvas = MyDocument.createElement('canvas', {
      parent: container, where: 'afterend',
      classList: container.classList,
      tabIndex: 1, fireRightClick: true,
    });
    let fabric_canvas = new fabric.Canvas(canvas);
    canvas.nextSibling.oncontextmenu=(ev)=>{
      ev.preventDefault();
      if(confirm('Download picture?')) this.download(canvas);
    };
    let script = container.innerText;
    var f = eval(script);
    await f(fabric_canvas);
    return 
  }

  serialize(canvas){
    return canvas.toDataURL({
       width: canvas.width, height: canvas.height,
       left: 0, top: 0, format: 'png',
    });
  }

  download(canvas){
    const link = document.createElement('a');
    link.download = 'image.png';
    link.href = this.serialize(canvas);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}

var pluginFabric = new PluginFabric();


// --- caph-fabric ---


class CaphFabric{}

CaphFabric._Base = class{
  container = null;
  async draw_on(canvas, {width, height, top=0, left=0, borders=false}={}){
    assert(width&&height);
    canvas.setHeight(height);
    canvas.setWidth(width);
    //2 pixels padding for seing borders
    height = Math.max(height-2, 0);
    width = Math.max(width-2, 0);
    this._draw_on(canvas, {width, height, top, left, borders});
  }
  make_border(){
    return new fabric.Rect({
      fill: '#eeffdd11', stroke:'black',
      width: this.container.width, height: this.container.height,
      top: this.container.top, left: this.container.left,
      selectable: false,
    });
  }
  get center(){
    return {
      x: this.container.left+this.container.width/2,
      y: this.container.top+this.container.height/2,
    }
  }
}

CaphFabric.Atom = class extends CaphFabric._Base{
  element=null;

  _draw_on(canvas, {width, height, top, left, borders}){
    this.container = {width, height, top, left};
    if(borders) canvas.add(this.make_border());
    this.element.set({ // center
      top: top+(height-this.height)/2,
      left: left+(width-this.width)/2,
    });
    canvas.add(this.element);
  }

  async load_svg(svg_string){
    return new Promise((ok, err)=>{
      fabric.loadSVGFromString(svg_string, (objs, opts)=>{
        var obj;
        try{ obj = fabric.util.groupSVGElements(objs, opts);}
        catch(e){ err(e); }
        if(obj){
          ok(obj);
        }
      });
    });
  }

  async load_tex(formula, {scale=1}={}){
    formula = formula||'';
    const e = MathJax.tex2svg(formula).firstElementChild;
    ['height', 'width'].forEach((s)=>{
      let v = e.getAttribute(s).replace('ex', '');
      v = parseFloat(v)*0.522*scale + 'em';
      e.setAttribute(s, v);
    });
    return await this.load_svg(e.outerHTML);
  }

  async load_content(){
    let text = await this.load_tex(this.text);
    
    let padding=null, margin=null, fill='#ffeeaa', stroke='black', box_props={};

    let width = text.width*text.scaleX;
    let height = text.height*text.scaleY;
    if(padding==null) padding = 5+Math.log1p(height*width);
    if(margin==null) margin = 0;
    text.set({top:margin+padding, left:margin+padding});

    let rect = new fabric.Rect({
      left:margin, top:margin,
      width: width+2*padding,
      height: height+2*padding,
      fill:fill, stroke:stroke, ...box_props
    });
    let box = new fabric.Group([rect, text]);
    box.set({
      width: 2*margin+rect.width,
      height: 2*margin+rect.height,
    });
    this.element = box;
  }
  get width(){
    assert(this.element);
    return this.element.width*this.element.scaleX;
  }
  get height(){
    assert(this.element);
    return this.element.height*this.element.scaleY;
  }
  get left(){
    assert(this.element);
    return this.element.left;
  }
  get top(){
    assert(this.element);
    return this.element.top;
  }
  side_midpoint(side){
    switch (side) {
      case 'top':
        return [this.left+this.width/2, this.top];
      case 'bottom':
        return [this.left+this.width/2, this.top+this.height];
      case 'left':
        return [this.left, this.top+this.height/2];
      case 'right':
        return [this.left+this.width, this.top+this.height/2];
      default:
        throw new Error();
    }
  }
  get center(){
    return [this.left+this.width/2, this.top+this.height/2];
  }

}


CaphFabric.Leaf = class extends CaphFabric.Atom{
  arrows = [];
  constructor(text='', {
      padding=null, fill='#ffeeaa88', stroke='black',
      top=null, left=null, ...box_props}={}){
    super();
    this.text=text;
  }
}

CaphFabric.Arrow = class extends CaphFabric.Atom{
  constructor(from, to, from_side, to_side, {text, stroke, ...props}={}){
    assert(from && from_side);
    assert(to && to_side);
    super();
    this.from = {node:from, side:from_side};
    this.to = {node:to, side:to_side};
    this.text=text;
    this.stroke = stroke||'black';
    this.props=props;
  }
  draw_on(canvas){
    this.from.point = ()=>this.from.node.side_midpoint(this.from.side);
    this.to.point = ()=>this.to.node.side_midpoint(this.to.side);

    let [x1,y1] = this.from.point();
    let [x2,y2] = this.to.point();
    let line = new fabric.Line([x1,y1,x2,y2],{
      stroke: this.stroke, selectable:false,
    });

    this.from.node.element.on('moving', (e)=>{
      let [x1,y1] = this.from.point();
      line.set({x1, y1});
    });
    this.to.node.element.on('moving', (e)=>{
      let [x2,y2] = this.to.point();
      line.set({x2, y2});
    });
    canvas.add(line);

    let r=5;
    let originX, originY;
    let angle = Math.atan2((y1-y2),(x1-x2))*180/Math.PI-90;
    let deltaX=-r*Math.sin(angle*Math.PI/180);
    let deltaY=r*Math.cos(angle*Math.PI/180);

    let triangle = new fabric.Triangle({
        left: x2 + deltaX,
        top: y2 + deltaY,
        originX: 'center',
        originY: 'center',
        selectable: false,
        pointType: 'arrow_start',
        angle: angle,
        width: 2*r, height: 2*r,
        fill: this.stroke,
    });
    canvas.add(triangle);
  }
}

CaphFabric.Tree = class extends CaphFabric._Base{
  direction = 'row'; //only row or column

  constructor(...children){
    super();
    this.children = this.validate_trees(children);
  }
  get elements(){
    return this.children.map(x=>x.element);
  }
  validate_trees(trees){
    for(let e of trees){
      if(e instanceof CaphFabric.Tree) ;
      else if(e instanceof CaphFabric.Leaf);
      else{
        console.error(e);
        throw 'Object must be Tree or Leaf';
      }
    }  
    return trees;
  }

  _draw_on(canvas, {width, height, top, left, borders}){
    this.container = {width, height, top, left};
    if(borders) canvas.add(this.make_border());
    let w, h, dw, dh, aux;
    if(this.direction=='row'){
      let W = this.width;
      w = dw = (e)=>e.width/W*width;
      h = (e)=>height;
      dh = (e)=>0;
    } else if (this.direction=='column'){
      let H = this.height;
      h = dh = (e)=>e.height/H*height;
      w = (e)=>width;
      dw = (e)=>0;
    } else throw new Error();

    for(let e of this.children){
      e._draw_on(canvas, {top: top, left: left, width:w(e), height:h(e), borders });
      left+=dw(e); top+=dh(e);
    }
  }

  get width(){
    let v;
    if(this.direction=='row')
      v = this.children.reduce((p,c)=>p+c.width, 0);
    else if(this.direction=='column')
      v = this.children.reduce((p,c)=>Math.max(p,c.width), 0);
    else throw new Error();
    return v;
  }
  get height(){
    let v;
    if(this.direction=='column')
      v = this.children.reduce((p,c)=>p+c.height, 0);
    else if(this.direction=='row')
      v = this.children.reduce((p,c)=>Math.max(p,c.height), 0);
    else throw new Error();
    return v;
  }
}
CaphFabric.HTree = CaphFabric.Tree;
CaphFabric.VTree = class extends CaphFabric.Tree{
  direction='column';
}

CaphFabric.functions = {
  HTree: (...args)=>new CaphFabric.HTree(...args),
  VTree: (...args)=>new CaphFabric.VTree(...args),
  Node: (...args)=>new CaphFabric.Leaf(...args),
  Edge: (...args)=>new CaphFabric.Arrow(...args),
  draw: async function (canvas, {layout, width, height,
      nodes={}, arrows=[], borders=false}={}){
    let lnodes = Object.values(nodes);
    layout = layout || new CaphFabric.Tree(lnodes);
    width = width || 500;
    height = height || width;
    for(let u of lnodes) await u.load_content();
    for(let a of arrows) await a.load_content();
    await layout.draw_on(canvas, {width, height, borders});
    for(let a of arrows) await a.draw_on(canvas);
  }
}
